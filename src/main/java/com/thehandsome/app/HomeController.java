package com.thehandsome.app;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.thehandsome.app.dto.MemberDTO;
import com.thehandsome.app.dto.ProductDTO;
import com.thehandsome.app.service.RecommendService;
import com.thehandsome.app.utils.RecommendProduct;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class HomeController {
 
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private RecommendService recommendService;

	RecommendProduct rp = new RecommendProduct();

	@GetMapping("/")
	public String main(Model model, HttpSession session) throws Exception {

		List<ProductDTO> recommendList = new LinkedList<>();

		MemberDTO memberInfo = (MemberDTO) session.getAttribute("memberInfo");

		if (memberInfo == null) {

			recommendList = recommendService.getNotMemberRecommend();

		} else {

			ProductDTO last_product = recommendService.getLastMemberActivity(memberInfo.getId());

			if (last_product == null) 
				recommendList = recommendService.getFirstMemberRecommend(memberInfo.getPrefer_brand_id());
			else {
				System.out.println("최근 상품   " + last_product.toString());

				List<String> list = rp.recommend(last_product.getId());

				Deque<ProductDTO> recommendList1 = new LinkedList<>();
				Deque<ProductDTO> recommendList2 = new LinkedList<>();
				Deque<ProductDTO> recommendList3 = new LinkedList<>();
				Deque<ProductDTO> recommendList4 = new LinkedList<>();

				/*
				 * [추천 상품 필터링]
				 * 0. 이미 구매한 상품 필터링 
				 * 1. 성별에 맞게 필터링 
				 * 2. 최근 조회한 상품의 옷 분류가 앞으로 
				 * 3. 최근 조회한 상품의 브랜드를 앞으로
				 */

				for (String id : list) {

					System.out.print(id + " ");

					ProductDTO item = recommendService.getProductDetail(id);

					if (item.getSex().equals(last_product.getSex())) {
						if (item.getMaincategory_id() == last_product.getMaincategory_id()) {
							if (item.getSubcategory_id() == last_product.getSubcategory_id()) {
								if (item.getBrand_id() == last_product.getBrand_id()) {
									recommendList1.offerLast(item);
									continue;
								} else {
									recommendList2.offerLast(item);
									continue;
								}
							} else {
								recommendList3.offerLast(item);
								continue;
							}

						} else {
							recommendList4.offerLast(item);
							continue;
						}
					}
				}

				recommendList1.addAll(recommendList2);
				recommendList1.addAll(recommendList3);
				recommendList1.addAll(recommendList4);

				for (int i = 0; i < 10; i++)
					recommendList.add(recommendList1.pollFirst());
			}
		}

		model.addAttribute("recommendList", recommendList);

		return "main";
	}

	@GetMapping("/recommend")
	public String recommend(Model model, HttpSession session) throws Exception {

		List<ProductDTO> recommendList = new LinkedList<>();
		
		int i = 0;

		MemberDTO memberInfo = (MemberDTO) session.getAttribute("memberInfo");

		if (memberInfo == null) {

			logger.info("비회원" + ++i);
			recommendList = recommendService.getNotMemberRecommend();

		} else {

			ProductDTO last_product = recommendService.getLastMemberActivity(memberInfo.getId());

			if (last_product == null) 
				recommendList = recommendService.getFirstMemberRecommend(memberInfo.getPrefer_brand_id());
			else {

				List<String> list = rp.recommend(last_product.getId());

				Deque<ProductDTO> recommendList1 = new LinkedList<>();
				Deque<ProductDTO> recommendList2 = new LinkedList<>();
				Deque<ProductDTO> recommendList3 = new LinkedList<>();
				Deque<ProductDTO> recommendList4 = new LinkedList<>();

	
				/*
				 * [추천 상품 필터링]
				 * 0. 이미 구매한 상품 필터링 
				 * 1. 성별에 맞게 필터링 
				 * 2. 최근 조회한 상품의 옷 분류가 앞으로
				 * 3. 최근 조회한 상품의 브랜드를 앞으로
				 */

				for (String id : list) {

					ProductDTO item = recommendService.getProductDetail(id);

					if (item.getSex().equals(last_product.getSex())) {
						if (item.getMaincategory_id() == last_product.getMaincategory_id()) {
							if (item.getSubcategory_id() == last_product.getSubcategory_id()) {
								if (item.getBrand_id() == last_product.getBrand_id()) {
									recommendList1.offerLast(item);
									continue;
								} else {
									recommendList2.offerLast(item);
									continue;
								}
							} else {
								recommendList3.offerLast(item);
								continue;
							}

						} else {
							recommendList4.offerLast(item);
							continue;
						}
					}
				}

				recommendList1.addAll(recommendList2);
				recommendList1.addAll(recommendList3);
				recommendList1.addAll(recommendList4);

				recommendList.addAll(recommendList1);

			}
		}

		model.addAttribute("recommendList", recommendList);

		return "productlist/recommend";
	}
}
