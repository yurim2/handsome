/*create table
===================================*/
-- 테이블, 제약조건 및 뷰 삭제
DROP TABLE productcolor CASCADE CONSTRAINTS;
DROP TABLE productsize CASCADE CONSTRAINTS;
DROP TABLE product CASCADE CONSTRAINTS;
DROP TABLE maincategory CASCADE CONSTRAINTS;
DROP TABLE subcategory CASCADE CONSTRAINTS;
DROP TABLE gendersubcategory CASCADE CONSTRAINTS;
DROP TABLE brand CASCADE CONSTRAINTS;
DROP TABLE department CASCADE CONSTRAINTS;
DROP TABLE branch CASCADE CONSTRAINTS;
DROP TABLE ADMIN CASCADE CONSTRAINTS;
DROP TABLE MEMBER CASCADE CONSTRAINTS;
DROP TABLE cart CASCADE CONSTRAINTS;
DROP TABLE BUYING_HISTORY CASCADE CONSTRAINTS;
DROP TABLE STOCK CASCADE CONSTRAINTS;
DROP TABLE REVIEW CASCADE CONSTRAINTS;
DROP TABLE USER_ACTIVITY CASCADE CONSTRAINTS;
DROP TABLE IMPORT_AND_EXPORT CASCADE CONSTRAINTS;


-- 테이블 생성 
-- PRODUCT 테이블 생성 
CREATE TABLE product (
    ID               VARCHAR2(255) NOT NULL,
    brand_id         NUMBER(10) NOT NULL,
    sex              VARCHAR2(255) NOT NULL,
    maincategory_id  NUMBER(10) NOT NULL,
    subcategory_id   NUMBER(10) NOT NULL,
    title            VARCHAR2(255) NOT NULL,
    price            NUMBER(20) NOT NULL,
    longdetail       CLOB NOT NULL,
    shortdetail      CLOB NOT NULL,
    searched         NUMBER DEFAULT 0 NOT NULL
    /*****************************************************************************
      TABLE NAME  	: PRODUCT 
      DESCRIPTION   : 상품 정보를 저장하는 테이블
      PARAMETERS    : id     -- 상품 번호
					  brand_id   -- 브랜드 이름
					  sex  -- 상품 카테고리 성별
					  maincategory_id  --  상품 메인 카테고리 번호
					  subcategory_id   -- 상품 서브 카테고리 번호
					  title  --  상품 이름
					  price  --  상품 가격
					  longdetail   --  상품 긴 설명
					  shortdetail  --  상품 짧은 설명
					  searched     -- 해당 상품이 검색된 횟수
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-05    IYR              최초 작성
     2021-07-07    IYR                수정
     2021-07-07    HSH           검색 횟수 컬럼 추가
	

******************************************************************************/
);

ALTER TABLE product ADD CONSTRAINT product_pk PRIMARY KEY ( ID );

-- PRODUCTCOLOR 테이블 생성 
CREATE TABLE productcolor (
    product_id  VARCHAR2(255) NOT NULL,
    color      VARCHAR2(255) NOT NULL,
    imagepath  VARCHAR2(255) NOT NULL
   
      /*****************************************************************************
      TABLE NAME  	: PRODUCTCOLOR  
      DESCRIPTION   : 상품 색상을 저장하는 테이블
      PARAMETERS    : productId  --   상품 번호
					  color     --   상품 색상
					  imagePath	 --   상품 이미지
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-05    IYR              최초 작성
	 2021-07-12    CMJ				컬럼명 수정

******************************************************************************/
);

ALTER TABLE productcolor ADD CONSTRAINT productcolor_pk PRIMARY KEY ( product_id,
                                                                      color );
-- PRODUCTSIZE 테이블 생성 
CREATE TABLE productsize (
    product_id  VARCHAR2(255) NOT NULL,
    sizelabel  VARCHAR2(255) NOT NULL
    
    /*****************************************************************************
      TABLE NAME  	: PRODUCTCOLOR  
      DESCRIPTION   : 상품 색상을 저장하는 테이블
      PARAMETERS    : productId  --   상품 번호
					  sizelabel     --   상품 사이즈
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-05    IYR              최초 작성
	 2021-07-12    CMJ				컬럼명 수정

******************************************************************************/
);
ALTER TABLE productsize ADD CONSTRAINT productsize_pk PRIMARY KEY ( product_id,
                                                                    sizelabel );

-- MAINCOLOR 테이블 생성 
CREATE TABLE maincolor(
    id NUMBER GENERATED AS IDENTITY NOT NULL, 
    color VARCHAR2(30) NOT NULL
    
     /*****************************************************************************
      TABLE NAME  	: MAINCOLOR 
      DESCRIPTION   : 메인 색상 정보 테이블
      PARAMETERS    : id  --   색상 구분
                      color --  색상 헥사 정보
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-14    IYR              최초 작성
   
	

******************************************************************************/
    );
ALTER TABLE maincolor ADD CONSTRAINT maincolor_pk PRIMARY KEY (id);

-- SUBCOLOR테이블 생성 
CREATE TABLE subcolor(
    maincolor VARCHAR2(30) NOT NULL, 
    color VARCHAR2(30) NOT NULL
    
     /*****************************************************************************
      TABLE NAME  	: SUBCOLOR
      DESCRIPTION   : 서브 색상 정보 테이블
      PARAMETERS    : maincolor  --   메인 색상 정보
                      color --  색상 헥사 정보
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-14    IYR              최초 작성
   
	

******************************************************************************/
    );
ALTER TABLE subcolor ADD CONSTRAINT subcolor_pk PRIMARY KEY (maincolor, color);


-- MAINCATEGORY 테이블 생성 
CREATE TABLE maincategory (
    id NUMBER GENERATED AS IDENTITY NOT NULL, 
    name VARCHAR2(30) NOT NULL, 
    ename VARCHAR2(30) NOT NULL
    
     /*****************************************************************************
      TABLE NAME  	: MAINCATEGORY
      DESCRIPTION   : 메인 카테고리 정보 테이블
      PARAMETERS    : id  --   카테고리 구분
                      name  --  카테고리 한글표기
					  ename     --  카테고리 영어표기
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-05    CMJ              최초 작성
     2017-07-12    KDH              대소문자 구별
	

******************************************************************************/
    );
ALTER TABLE maincategory ADD CONSTRAINT maincategory_pk PRIMARY KEY (id);

-- SUBCATEGORY 테이블 생성 
CREATE TABLE subcategory (
    id NUMBER GENERATED AS IDENTITY NOT NULL, 
    name VARCHAR2(30) NOT NULL, 
    ename VARCHAR2(30) NOT NULL
    
     /*****************************************************************************
      TABLE NAME  	: SUBCATEGORY
      DESCRIPTION   : 상세 카테고리 정보 테이블
      PARAMETERS    : id  --   카테고리 구분
                      name  --  카테고리 한글표기
					  ename     --  카테고리 영어표기
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-05    CMJ              최초 작성
	

******************************************************************************/
    );
ALTER TABLE subcategory ADD CONSTRAINT subcategory_pk PRIMARY KEY (id);


-- BRAND 테이블 생성
CREATE TABLE brand(
    id    NUMBER GENERATED AS IDENTITY NOT NULL,
    name  VARCHAR2(100) NOT NULL
    
    /*****************************************************************************
      TABLE NAME  	: BRAND  
      DESCRIPTION   : 브랜드 이름을 저장하는 테이블
      PARAMETERS    : ID    --  브랜드 번호
					  NAME	--	브랜드 이름
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-05    KDH              최초 작성
	

******************************************************************************/
);
ALTER TABLE brand ADD CONSTRAINT brand_id_pk PRIMARY KEY(id);

-- gendersubcategory 테이블 생성 
CREATE TABLE gendersubcategory (
    gender VARCHAR2(30) NOT NULL, 
    subcategory_id NUMBER(10) NOT NULL
    
     /*****************************************************************************
      TABLE NAME  	: gendersubcategory
      DESCRIPTION   : 성별 & 서브 카테고리 정보 테이블
      PARAMETERS    : gender  --   성별 구분
                      subcategory_id  --  서브 카테고리 번호

      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-10    IYR              최초 작성
	 2021-07-12    CMJ				컬럼명 수정

******************************************************************************/
    );
ALTER TABLE gendersubcategory ADD CONSTRAINT gendersubcategory_pk PRIMARY KEY (gender, subcategory_id);

-- DEPARTMENT 테이블 생성
CREATE TABLE department(
    id        NUMBER GENERATED AS IDENTITY NOT NULL,
    name      VARCHAR2(100) NOT NULL,
    address   VARCHAR2(200) NOT NULL,
    latitude  NUMBER        NOT NULL,
    longitude NUMBER        NOT NULL
    
    /*****************************************************************************
      TABLE NAME  	: DEPARTMENT  
      DESCRIPTION   : 백화점 정보를 저장하는 테이블
      PARAMETERS    : ID        --  백화점 번호
					  NAME		--	백화점 이름
					  ADDRESS	--	백화점 주소
					  LATITUDE  --  백화점 위도
					  LONGITUDE --  백화점 경도
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-05    KDH              최초 작성
	

******************************************************************************/
);
ALTER TABLE department ADD CONSTRAINT department_id_pk PRIMARY KEY(id);



-- BRANCH 테이블 생성
CREATE TABLE branch(
    id              NUMBER GENERATED AS IDENTITY NOT NULL,
    department_id   NUMBER NOT NULL,
    brand_id        NUMBER NOT NULL
    
    /*****************************************************************************
      TABLE NAME  	: DEPARTMENT  
      DESCRIPTION   : 백화점 정보를 저장하는 테이블
      PARAMETERS    : id            --  지점 번호
					  department_id	--	백화점 번호
					  brand_id		--	브랜드 번호
      특이사항       : 백화점 별로 여러개의 브랜드를 가지고 있음.
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-05    KDH              최초 작성
     2021-07-12    KDH              branch.ID -> branch.id
	

******************************************************************************/
);
ALTER TABLE branch ADD CONSTRAINT branch_id_pk PRIMARY KEY(id);


-- ADMIN 테이블 생성
CREATE TABLE ADMIN(
    id              VARCHAR2(100) NOT NULL,
    password        VARCHAR2(1000) NOT NULL,
    branch_id       NUMBER
    
    /*****************************************************************************
      TABLE NAME  	: DEPARTMENT  
      DESCRIPTION   : 백화점 정보를 저장하는 테이블
      PARAMETERS    : id			--	아이디
					  password		--	비밀번호
					  branch_id     --  지점 번호
      특이사항       : 
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-05    KDH              최초 작성
	

******************************************************************************/
);
ALTER TABLE ADMIN ADD CONSTRAINT admin_id_pk PRIMARY KEY(id);



-- MEMBER 테이블 생성
CREATE TABLE MEMBER (
    id          VARCHAR2(100) NOT NULL,
    password    VARCHAR2(1000) NOT NULL,
    name        VARCHAR2(50) NOT NULL,
    birth       VARCHAR2(100) NOT NULL,
    age         NUMBER(10) NOT NULL,
    gender      VARCHAR2(50) NOT NULL,
    address     VARCHAR2(100) NOT NULL,
    phone       VARCHAR2(100) NOT NULL,
    prefer_department_id      NUMBER(10) NOT NULL,
    prefer_brand_id       NUMBER(10) NOT NULL
    /*****************************************************************************
      TABLE NAME  	: MEMBER 
      DESCRIPTION   : 고객의 정보를 나타내는 테이블
      PARAMETERS    : id       -- 고객 아이디
                      password -- 고객 비밀번호
                      name     -- 이름
                      birth       -- 생일
                      age       -- 나이
                      gender  -- 성별
                      address  -- 주소
                      phone  -- 연락처
                      preferDepartment_id  -- 선호하는 백화점 지점
                      preferBrand_id  -- 선호하는 브랜드
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-06    CMJ              최초 작성
	 2021-07-08    IYR                수정

******************************************************************************/
);

ALTER TABLE MEMBER ADD CONSTRAINT member_pk PRIMARY KEY (id);


-- CART 테이블 생성
CREATE TABLE cart (
    user_id            VARCHAR2(100) NOT NULL,
    product_id         VARCHAR2(255) NOT NULL,
    quantity           NUMBER(10) NOT NULL,
    sizelabel          VARCHAR2(255) NOT NULL,
    color              VARCHAR2(255) NOT NULL
    /*****************************************************************************
      TABLE NAME  	: CART 
      DESCRIPTION   : 고객의 장바구니 정보를 나타내는 테이블
      PARAMETERS    : user_id     -- 고객 아이디
					  product_id  -- 상품 고유번호
					  quantity    -- 상품 수량
					  sizelabel   -- 상품 사이즈
					  color		  -- 상품 색상
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-06    CMJ              최초 작성
	 2021-07-12    CMJ				컬럼명 수정

******************************************************************************/
);

ALTER TABLE cart ADD CONSTRAINT cart_pk PRIMARY KEY ( user_id, product_id );
	

-- BUYING_HISTORY 테이블 생성
CREATE TABLE BUYING_HISTORY(
    id                NUMBER GENERATED AS IDENTITY NOT NULL,
    member_id         VARCHAR2(100) NOT NULL,
    product_id        VARCHAR2(255) NOT NULL,
    department_id     NUMBER NOT NULL,
    brand_id          NUMBER NOT NULL,
    purchase_date     DATE NOT NULL,
    quantity          NUMBER(2) NOT NULL,
    sizelabel         VARCHAR2(255) NOT NULL,
    color             VARCHAR2(255) NOT NULL,
    rec_method        VARCHAR2(100) NOT NULL,
    stock             NUMBER(1) NOT NULL,
    complete          NUMBER(1) NOT NULL
    
    /*****************************************************************************
      TABLE NAME  	: BUYING_HISTORY  
      DESCRIPTION   : 구매 정보를 저장하는 테이블
      PARAMETERS    : id			--	아이디
					  member_id		--	구매자 식별자
					  product_id	--	상품 식별자
					  department_id --  백화점 식별자
					  brand_id		--	브랜드 식별자
					  quantity		--	수량
					  sizelabel		--	사이즈
					  color			-- 	색상
					  rec_method	--  수령 방법
					  stock			--	재고 식별자
					  complete		--	상태 식별자
      특이사항       : 
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-06    KDH              최초 작성
	

******************************************************************************/
);
ALTER TABLE BUYING_HISTORY ADD CONSTRAINT buying_history_id_pk PRIMARY KEY(id);     

-- STOCK 테이블 생성
CREATE TABLE STOCK(
    id                NUMBER GENERATED AS IDENTITY NOT NULL,
    branch_id         NUMBER NOT NULL,
    product_id        VARCHAR2(255) NOT NULL,
    sizelabel         VARCHAR2(255) NOT NULL,
    color             VARCHAR2(255) NOT NULL,
    quantity          NUMBER(2)     NOT NULL
    
    /*****************************************************************************
      TABLE NAME  	: STOCK  
      DESCRIPTION   : 재고 정보를 저장하는 테이블
      PARAMETERS    : id            --  식별자
                      branch_id     --  지점 식별자
                      product_id    --  상품 식별자
                      sizelabel     --  사이즈
                      color         --  색상
                      quantity      --  수량
      특이사항       : 
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-06    KDH              최초 작성
     2021-07-12    KDH              ID Column 추가, PK 변경
	

******************************************************************************/
);
ALTER TABLE STOCK ADD CONSTRAINT stock_id_pk PRIMARY KEY(id);

-- REVIEW 테이블 생성
CREATE TABLE REVIEW(
     id          NUMBER GENERATED AS IDENTITY NOT NULL, 
     member_id   VARCHAR2(100) NOT NULL,
     product_id  VARCHAR2(255) NOT NULL,
     rating      NUMBER NOT NULL,
     comments    CLOB NOT NULL
     /*****************************************************************************
      TABLE NAME  	: REVIEW 
      DESCRIPTION   : 리뷰를 저장하는 테이블
      PARAMETERS    : id          -- 리뷰 번호
					  member_id	  -- 리뷰 작성자 ID
					  product_id  -- 작성된 상품 ID
					  rating      -- 별점
					  comments    -- 리뷰 내용
      특이사항       :
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-07    HSH              최초 작성

******************************************************************************/
);

ALTER TABLE REVIEW ADD CONSTRAINT REVIEW_PK PRIMARY KEY (id);

-- USER_ACTIVITY 테이블 생성
CREATE TABLE USER_ACTIVITY(
    id          NUMBER GENERATED AS IDENTITY NOT NULL,
    member_id   VARCHAR2(100) NOT NULL,
    product_id  VARCHAR2(255) NOT NULL,
    last_date   DATE NOT NULL,
    rating      NUMBER,
    click_num   NUMBER,
    stay_time   NUMBER,
    is_like     NUMBER,
    p_rating    NUMBER,
    p_click_num NUMBER,
    p_stay_num NUMBER,
    preference  NUMBER NOT NULL
    
    /*****************************************************************************
      TABLE NAME  	: USER_ACTIVITY  
      DESCRIPTION   : 회원 행동 데이터를 저장하는 테이블
      PARAMETERS    : id			--	아이디
					  member_id		--	회원 식별자
					  product_id	--	상품 식별자
					  last_date     --  마지막 행동 데이터 추가/변경 날짜
					  rating		--	리뷰 평점
					  click_num		--	해당 상품 페이지 클릭 횟수
					  stay_time		--	해당 상품 페이지 체류 시간
					  is_like	    -- 	해당 상품 찜 유무
					  p_rating	    --  리뷰 평점 행위 비율
					  p_click_num	--	해당 상품 페이지 클릭 횟수 행위 비율
					  p_stay_num	--	해당 상품 페이지 체류 시간 행위 비율
					  preference    --	선호도
      특이사항       : 
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-07    HSH              최초 작성
     2021-07-07    HSH              P_IS_LIKE 컬럼 삭제
     2021-07-07    HSH              측정 값 컬럼 null 허용
	

******************************************************************************/
);

ALTER TABLE USER_ACTIVITY ADD CONSTRAINT USER_ACTIVITY_PK PRIMARY KEY (id);
	

-- IMPORT_AND_EXPORT 테이블 생성
CREATE TABLE IMPORT_AND_EXPORT(
    buying_history_id NUMBER NOT NULL,
    departure         NUMBER NOT NULL,
    destination       NUMBER NOT NULL,
    state             VARCHAR2(20) NOT NULL

    /*****************************************************************************
      TABLE NAME  	: IMPORT_AND_EXPORT
      DESCRIPTION   : 입고/출고/발송 정보를 저장하는 테이블
      PARAMETERS    : buying_history_id   --  구매이력 ID
					  departure           --  출발 Branch
					  destination         --  도착 Branch
					  state			      --  상태 정보
      특이사항       : state -> 발송 요청, 출고 요청, 입고 완료, 입고 요청, 발송 완료
  ----------------------------------------------------------------------------
        DATE       AUTHOR           DESCRIPTION
  ----------------------------------------------------------------------------
     2021-07-08    KDH              최초 작성
	

******************************************************************************/
);
ALTER TABLE IMPORT_AND_EXPORT ADD CONSTRAINT IMPORT_AND_EXPORT_ID_PK PRIMARY KEY(buying_history_id); 

--------------------------------------------------------------------------------------------

ALTER TABLE productcolor
    ADD CONSTRAINT productcolor_product_fk FOREIGN KEY ( product_id )
        REFERENCES product ( id );

ALTER TABLE productsize
    ADD CONSTRAINT productsize_product_fk FOREIGN KEY ( product_id )
        REFERENCES product ( id );
        
ALTER TABLE branch ADD CONSTRAINT branch_department_id_fk 
                      FOREIGN KEY(department_id) REFERENCES department(id);
                      
ALTER TABLE branch ADD CONSTRAINT branch_brand_id_fk 
                      FOREIGN KEY(brand_id) REFERENCES brand(id);
                      
ALTER TABLE ADMIN ADD CONSTRAINT admin_branch_id_fk 
                      FOREIGN KEY(branch_id) REFERENCES branch(id);
                      
ALTER TABLE BUYING_HISTORY ADD CONSTRAINT buying_history_member_id_fk 
                               FOREIGN KEY(member_id) REFERENCES member(id);                      

ALTER TABLE BUYING_HISTORY ADD CONSTRAINT buying_history_product_id_fk 
                               FOREIGN KEY(product_id) REFERENCES product(id);
                      
ALTER TABLE BUYING_HISTORY ADD CONSTRAINT buying_history_department_id_fk 
                               FOREIGN KEY(department_id) REFERENCES department(id);                    
                      					 
ALTER TABLE BUYING_HISTORY ADD CONSTRAINT buying_history_brand_id_fk 
                               FOREIGN KEY(brand_id) REFERENCES brand(id);
                               
ALTER TABLE STOCK ADD CONSTRAINT stock_branch_id_fk 
                      FOREIGN KEY(branch_id) REFERENCES branch(id);
                                                              
ALTER TABLE STOCK ADD CONSTRAINT stock_product_id_fk 
                      FOREIGN KEY(product_id) REFERENCES product(id);
                      
ALTER TABLE REVIEW
    ADD CONSTRAINT REVIEW_MEMBER_FK FOREIGN KEY (member_id)
        REFERENCES MEMBER(id);
        
ALTER TABLE REVIEW
    ADD CONSTRAINT REVIEW_PRODUCT_FK FOREIGN KEY (product_id)
        REFERENCES PRODUCT(id);
        
ALTER TABLE USER_ACTIVITY
    ADD CONSTRAINT USER_ACTIVITY_MEMBER_FK FOREIGN KEY (member_id)
        REFERENCES MEMBER(id);
        
ALTER TABLE USER_ACTIVITY
    ADD CONSTRAINT USER_ACTIVITY_PRODUCT_FK FOREIGN KEY (product_id)
        REFERENCES PRODUCT(id);
        
ALTER TABLE IMPORT_AND_EXPORT
    ADD CONSTRAINT IMPORT_AND_EXPORT_BUYING_HISTORY_FK FOREIGN KEY (buying_history_id)
        REFERENCES BUYING_HISTORY(id);
 

 